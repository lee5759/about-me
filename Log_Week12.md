|  |  |
| ------ | ------ |
| Logbook no. | 10 |
| Name | Abdul Rahman Lee |
| Matrix No | 199866 |
| Year | 4 |
| Subsystem | Propulsion and Power |
| Group no. | 5 |
| Date | 10 - 14 January 2021 |

| Target | Details |
| ------ | ------ |
| Agenda | Main focus was only on the report writing. Compacting the report and graph adjustments for better coherence of the report. |
| Decisions | Each members that were assigned for the report need to compact their respective section. |
