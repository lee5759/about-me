|  |  |
| ------ | ------ |
| Logbook no. | 09 |
| Name | Abdul Rahman Lee |
| Matrix No | 199866 |
| Year | 4 |
| Subsystem | Propulsion and Power |
| Group no. | 5 |
| Date | 3 - 7 January 2021 |

| Target | Details |
| ------ | ------ |
| Agenda | Analyse data from week 9 and week 10 for the thrust as well as continue on report writing and resume. |
