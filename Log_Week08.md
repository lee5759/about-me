|  |  |
| ------ | ------ |
| Logbook no. | 06 |
| Name | Abdul Rahman Lee |
| Matrix No | 199866 |
| Year | 4 |
| Subsystem | Propulsion and Power |
| Group no. | 5 |
| Date | 13/12/2021 - 17/12/2021 |

| Target | Details |
| ------ | ------ |
| Agenda | For propulsion subsystem, we were told that the thrust test was not necessary for the new motor. As for the group, there were no tasks assigned. |
| Goals | Wait for the new motor to arrive. |
| Decisions | Propulsion group decided not to run the thrust test for the new motor as it is concern that the new motor will get burn. |
| Justification during problem solving | We manage to gain approval from Dr. Salah to run the test on the new motor at only 30%. |
| Next Step | Run thrust test for the new motor to get better picture of the performance of the new motor. |
