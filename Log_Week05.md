|  |  |
| ------ | ------ |
| Logbook no. | 02 |
| Name | Abdul Rahman Lee |
| Matrix No | 199866 |
| Year | 4 |
| Subsystem | Propulsion and Power |
| Group no. | 5 |
| Date | 8-12 November 2021 |

| Target | Details |
| ------ | ------ |
| Agenda | Calibration of the test system. Tested Dr. Ezanee's motor. Joining Dr Ezanee's extra class with some input from him. Do the trolley test with the airship. |
| Goals | To get the right size of propeller suit for the project. Learn extra knowledge of propulsion from Dr. Ezanee. Get the rough numbers on amount of thrusts that will be needed to drive the airship. |
| Decisions | Deciding on the members that should join the lab test. Deciding the best way to do the trolley test to avoid errors. |
| Method | Decided the team members that are going for the thrust test through voluntary. Using trolley and spring balance to estimate the force needed. |
| Justification during problem solving | 2 |
| Impact | Conducting the test need to be precise and should be done at a smooth surface to reduce the friction of the trolley with the ground. Few students were not able to join the test due to clash of time and Lee was absent because of training. |
| Next Step | Continue with testing the airship with the trolley. If possible, test the new motor as well. |
