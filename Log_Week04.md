|  |  |
| ------ | ------ |
| Logbook no. | 02 |
| Name | Abdul Rahman Lee |
| Matrix No | 199866 |
| Year | 4 |
| Subsystem | Propulsion and Power |
| Group no. | 5 |
| Date | 1-5 November 2021 |

| Target | Details |
| ------ | ------ |
| Agenda | Test the old motor that we have and determine the amount of current used when the motor is working at a maximum thrust. |
| Goals | T get the right propeller and amount of current suits with the motor that's working at maximum thrust. |
| Decisions | We decided to test the motor with both 16 and 24 inches propeller. We also decided the member that willl go for the testing at Lab H2.3. |
| Method | Software used was RCbenhmark to test out the motor's thrust and store the data. |
| Impact | 24 inches propeller produced 2kg of thrust but the motor stopped working and one point and burned later after that, the reason was it might had reached the maximum capacity. |
| Next Step | Test out Dr. Ezanee's motor and do the trolley test with Dr. Ezanee and join his extra class. Also, we will be doing the calibration of the test system. |
