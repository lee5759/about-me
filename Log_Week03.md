|  |  |
| ------ | ------ |
| Logbook no. | 01 |
| Name | Abdul Rahman Lee |
| Matrix No | 199866 |
| Year | 4 |
| Subsystem | Propulsion and Power |
| Group no. | 5 |
| Date | 5 November 2021 |

| Target | Details |
| ------ | ------ |
| Agenda | Learned to use Gitlab, visual code and writing Markdown. Created the subsystem group’s Gantt Chart. Discussed on the timeline of the progress on thrust test to determine the following parameters.(Power analysis, Thrust, Current, Drag and RPM) |
| Goals | Familiarize with Gitlab and learn to write Markdown. To create Gantt Chart for the flow of works throughout the whole semester. Made appointment with Dr. Ezanee to perform the thrust tests. |
| Decisions | Deciding on what task we should do each week and mark it in the Gantt Chart. Deciding on the team members that will go for the thrust tests. Joined group study to familiarize with Gitlab. |
| Method | Decided the team members that are going for the thrust test through voluntary. |
| Justification during problem solving | 2 |
| Impact | Learning to use Gitlab is new to all of us but we managed to cop up with it. Few members were unable to attend the thrust test in the lab due to the pandemic. |
| Next Step | Performing thrust tests in Lab H2.3 on Tuesday for two different propeller sizes.|
