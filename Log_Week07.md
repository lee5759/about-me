|  |  |
| ------ | ------ |
| Logbook no. | 05 |
| Name | Abdul Rahman Lee |
| Matrix No | 199866 |
| Year | 4 |
| Subsystem | Propulsion and Power |
| Group no. | 5 |
| Date | 29 November - 3 December 2021 |

| Target | Details |
| ------ | ------ |
| Agenda | Online meeting with Dr. Ezanee regarding the possible choices for the landing of the airship. |
| Goals | To come out with the landing mechanism for the airship. |
| Decisions | Three options were given by Dr. Ezanee for us to give our thoughts on. Each options come with pros and cons. |
| Method | Online meeting through zoom meeting. |
| Justification during problem solving | - |
| Impact | Meeting were only attended by few group members but was overcome with meeting recording. Each options of landing mechanism have their own pros and cons that needs a thorough decision. |
| Next Step | - |
