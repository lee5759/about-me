|  |  |
| ------ | ------ |
| Logbook no. | 04 |
| Name | Abdul Rahman Lee |
| Matrix No | 199866 |
| Year | 4 |
| Subsystem | Propulsion and Power |
| Group no. | 5 |
| Date | 22-26 November 2021 |

| Target | Details |
| ------ | ------ |
| Agenda | Test the second old motor with power supply of 16V and battery 15.8V with ESC No. 4.  |
| Goals | To obtain the thrust that can be produce by the old motor using 17inch propeller. |
| Decisions | The decision needed to be done was the supply to the motor and the size of the propeller. We decided the memb that are able to attend the test in lab H2.3. |
| Method | Use of power suppply and battery supply. Volunteer for the test. |
| Justification during problem solving | - |
| Impact | Wrong orientation of the propeller caused lots of vibrations and unstable thrusts. Loose connection on the ESC made us having error in troubleshooting the current. |
| Next Step | Continue with testing the new motor. |
