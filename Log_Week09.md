|  |  |
| ------ | ------ |
| Logbook no. | 07 |
| Name | Abdul Rahman Lee |
| Matrix No | 199866 |
| Year | 4 |
| Subsystem | Propulsion and Power |
| Group no. | 5 |
| Date | 20 - 24 December 2021 |

| Target | Details |
| ------ | ------ |
| Agenda | No activities were done for this week as the classes were postponed due to sudden flood and covid-19 cases that arised in the UPM lab. Begun our report writing. |
