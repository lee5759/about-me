|  |  |
| ------ | ------ |
| Logbook no. | 08 |
| Name | Abdul Rahman Lee |
| Matrix No | 199866 |
| Year | 4 |
| Subsystem | Propulsion and Power |
| Group no. | 5 |
| Date |  27 Dec 2021 - 2 Jan 2022 |

| Target | Details |
| ------ | ------ |
| Agenda | Do report writing on the propulsion works weekly. Start to do our own resume for purpose of practise. Continue with completing the graph of new motor thrust testing. |
| Goals | To learn the proper way of making resume and weekly report.  |
| Decisions | - |
| Impact | - |
| Next Step | Backing up the other subsytem in connections and etc. |
